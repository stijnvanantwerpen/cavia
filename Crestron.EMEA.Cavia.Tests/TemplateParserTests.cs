﻿using Crestron.EMEA.Cavia.Components;
using Crestron.EMEA.I2P.Testtools;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Crestron.EMEA.Cavia.Tests
{
    [TestClass]
    public class TemplateParserTests
    {
        [TestMethod]
        public void Parse_HelloWorld()
        {
            var sutParser = new TemplateParser();
            const string template = "Hello %:World:";
            sutParser.LoadTemplate(template);
            sutParser.Set("World", "Parser");
            var result = sutParser.Parse();

            result.ShouldBe("Hello Parser");
        }

        [TestMethod]
        [Ignore]
        public void Parse_WithNonExistingPlaceholder()
        {
            var sutParser = new TemplateParser();
            const string template = "Hello %:World:";
            sutParser.LoadTemplate(template);            
            var result = sutParser.Parse();

            result.ShouldBe("Hello ");
        }
    }
}
