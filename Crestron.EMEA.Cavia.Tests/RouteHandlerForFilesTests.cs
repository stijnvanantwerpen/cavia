﻿using Crestron.EMEA.Cavia.Http;
using Crestron.EMEA.Cavia.Http.Cache;
using Crestron.EMEA.Cavia.Http.Routing;
using Crestron.EMEA.Cavia.IoC;
using Crestron.EMEA.I2P.Testtools;
using Crestron.EMEA.I2P.Tools.WinImp.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Crestron.EMEA.Cavia.Tests
{
    [TestClass]
    public class RouteHandlerForFilesTests
    {
        [TestMethod]
        public void Initialize()
        {
            var args = MockRepository.GenerateMock<HttpRequestArgs>();
            var factory = MockRepository.GenerateMock<CaviaFactory>();
            var requestContext = MockRepository.GenerateMock<RequestContext>();

            factory.Expect(e => e.CreateFileStreamCache()).Return(new FileStreamCache(new StreamFactoryForWindows()));            
            args.Expect(e => e.GetQueryStringVariable("noCache")).Return("true");

            var sut = new RouteHandlerForFiles(factory);
            var response = sut.CreateResponse(requestContext);
            
            response.ShouldNotBeNull();            
        }
    }
}
