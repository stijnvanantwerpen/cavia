﻿using Crestron.EMEA.Cavia.Cavia.Core;
using Crestron.EMEA.Cavia.IoC;
using Crestron.EMEA.I2P.Testtools;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Crestron.EMEA.Cavia.Tests
{
    [TestClass]
    public class CaviaServerTests
    {
        [TestMethod]
        public void Initialize()
        {            
            const ushort port = 8080;
            const string webFolder = "webFolder";
            const string binFolder = "binFolder";
            const string factoryName = "factoryName";

            var caviaFactoryProvider = MockRepository.GenerateMock<CaviaFactoryProvider>();
            var caviaFactory = MockRepository.GenerateMock<CaviaFactory>();
            var caviaCore = MockRepository.GenerateMock<CaviaCore>();
            var httpServer = MockRepository.GenerateMock<HttpServer>();            

            caviaFactory.Expect(e => e.CreateHttpServer()).Return(httpServer);
            caviaFactoryProvider.Expect(e => e.CreateFactory(binFolder, factoryName)).Return(caviaFactory);                    

            var sutCaviaServer = new CaviaServerImp(caviaCore, caviaFactoryProvider);
            sutCaviaServer.Init(port, webFolder, binFolder, factoryName);

            sutCaviaServer.Running.ShouldBe(false);
            httpServer.AssertWasCalled(x => x.Port = port);
        }
    }
}
