﻿using System;
using System.IO;
using Crestron.EMEA.Cavia.Http.Cache;
using Crestron.EMEA.I2P.Testtools;
using Crestron.EMEA.I2P.Tools.WinImp.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Crestron.EMEA.Cavia.Tests
{
    [TestClass]
    public class FileStreamCacheTests
    {
        [TestMethod]
        public void ReadUncachedFileFromDisk()
        {
            var filename = CreateTempFileForTest();

            var sutFileStreamCache = new FileStreamCache(new StreamFactoryForWindows());
            var memStream = sutFileStreamCache.GetFile(filename);

            memStream.Length.ShouldBe(1000);
            
            File.Delete(filename);
        }

        private static string CreateTempFileForTest()
        {
            var tempFileName = Path.GetTempFileName();
            using (var fileStream = new FileStream(tempFileName, FileMode.Open))
            {
                var rnd = new Random();
                for (var i = 0; i < 1000; i++)
                {
                    fileStream.WriteByte((byte) rnd.Next());
                }
                fileStream.Flush();
                fileStream.Close();
            }
            return tempFileName;
        }
    }
}
