﻿using System.Collections.Generic;
using Crestron.EMEA.Cavia.Cavia.SDK;
using Crestron.EMEA.Cavia.Http.Routing;
using Crestron.EMEA.Cavia.IoC;

namespace Crestron.EMEA.Cavia.Rijmenam
{
    public class CaviaFactoryForRijmenam : BaseCaviaFactory
    {
        public override CaviaAuthenticationProvider CreateAuthenticationProvider()
        {
            return new AuthenticationProviderForRijmenam();
        }
                     
        public override IEnumerable<RouteHandler> CreateRouteHandlers()
        {
            yield return new RouteHandlerForRijmenamLogin(this);
#pragma warning disable 1911
            var defaultRouteHandlers = base.CreateRouteHandlers();
#pragma warning restore 1911
            foreach (var rh in defaultRouteHandlers)
            {
                yield return rh;
            }            
        }

        public override string GetHello()
        {
            return "Hello, this is the Factory for Rijmenam, ready to serve!!!";
        }

        public override CaviaAreaExtender CreateAreaExtender()
        {
            return new RijmenamAreaExtender();
        }
    }
}