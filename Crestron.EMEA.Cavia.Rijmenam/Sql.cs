﻿using System;
using System.Data;
using System.Data.SqlClient;
using Crestron.EMEA.Cavia.DataLayer;
using Crestron.SimplSharp;

namespace Crestron.EMEA.Cavia.Rijmenam
{
    public class Sql
    {       
        private string ConnectionString
        {
            get
            {
                return "user id=login;" +
                       "password=audit;" +
                       "server=erp-server.adcrestron.eu;" +
                       "database=intranet; " +
                       "connection timeout=2";
            }
        }

        public UserDTO GetUser(string userId, string guid)
        {
            try
            {
                if (String.IsNullOrEmpty(userId) || String.IsNullOrEmpty(guid))
                {
                    return GetUser_CreateGuest();
                }

                using (var connection = new SqlConnection(ConnectionString))
                {
                    using (var command = new SqlCommand("dbo.get_TS_User", connection))
                    {                 
                        connection.Open();
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@userID", userId);
                        command.Parameters.AddWithValue("@guid", guid);
                        var reader = command.ExecuteReader();
                        var user = !reader.Read() ? GetUser_CreateGuest() : GetUser_CreateUser(reader);
                        connection.Close();
                        return user;
                    }
                }
            }
            catch (Exception e)
            {
                ErrorLog.Error("Error on SQL stored procedure, when running get_TS_User : " + e);
                throw;
            }
        }

        private static UserDTO GetUser_CreateUser(SqlDataReader reader)
        {
            string areaName = reader.GetValue(0).ToString();
            string allowed = reader.GetValue(1).ToString();
            string isAdmin = reader.GetValue(2).ToString();
            return new UserDTO
            {
                AreaNames  = areaName.ToUpper(),
                UserAllowed = allowed == "1" && areaName.Length > 0,
                IsAdmin = isAdmin == "1",
                IsGuest = false
            };
        }

        private static UserDTO GetUser_CreateGuest()
        {
            return new UserDTO
            {
                UserAllowed = true,
                IsAdmin = false,
                AreaNames = String.Empty,
                IsGuest = true
            };
        }
    }
}