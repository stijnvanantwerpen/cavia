﻿using System;
using System.Linq;
using Crestron.EMEA.Cavia.Cavia.SDK;
using Crestron.EMEA.Cavia.DataLayer;
using Crestron.EMEA.Cavia.Http.Tickets;
using Crestron.SimplSharp.Net;

namespace Crestron.EMEA.Cavia.Rijmenam
{
    public class AuthenticationProviderForRijmenam : CaviaAuthenticationProvider
    {
        public bool CanAuthenticate(HttpRequestArgs args)
        {     
            var referer = args.GetHeaderValueFor("Referer") ?? String.Empty;
            if (!referer.Contains("?")) return false;
            #region Magic Backdoor
            //TODO:Remove before Release
            if (!referer.Contains("magicBackdoorForDebug")) return true;
            #endregion
            return referer.Contains("rijmenamUserProperties=");
        }

        public UserDTO Authenticate(HttpRequestArgs args)
        {            
            var referer = args.GetHeaderValueFor("Referer");
            if (String.IsNullOrEmpty(referer)) return CreateGuest();            
            if (referer.Contains("magicBackdoorForDebug")) return CreateAdmin();

            var queryString = referer.Split('?').ElementAtOrDefault(1);
            var queryPairs = queryString.Split('&').Select(qp => { var pair = qp.Split('='); return new { Key = pair[0], Value = pair[1] }; });
            var rijmenamUserProperties = HttpUtility.UrlDecode(queryPairs.SingleOrDefault(qp => qp.Key == "rijmenamUserProperties").Value);            
            var ticket = new SecureTicket();
            ticket.LoadFromAuthenticationToken(rijmenamUserProperties);
            var userId = ticket.Get("userId");
            var guid = ticket.Get("guid");
                        
            var sql = new Sql();
            var user = sql.GetUser(userId, guid);
            user.FilterByAreaNames = true;
            user.AreaNames = user.AreaNames.ToUpper();
            return user;
        }     

        private UserDTO CreateGuest()
        {
            return new UserDTO
            {
                UserAllowed = true,
                IsAdmin = false,
                FilterByAreaNames = false,
                AreaNames = String.Empty,
                IsGuest = true
            };
        }

        private UserDTO CreateAdmin()
        {
            return new UserDTO
            {
                UserAllowed = true,
                IsAdmin = true,
                FilterByAreaNames = false,
                AreaNames = String.Empty,
                IsGuest = false
            };
        }
    }
}