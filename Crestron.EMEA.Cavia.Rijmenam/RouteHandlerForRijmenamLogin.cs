﻿using System;
using System.Collections.Generic;
using Crestron.EMEA.Cavia.Cavia.Core;
using Crestron.EMEA.Cavia.Http.Routing;
using Crestron.EMEA.Cavia.Http.Tickets;
using Crestron.EMEA.Cavia.IoC;
using Crestron.SimplSharp.Net;

namespace Crestron.EMEA.Cavia.Rijmenam
{
    public class RouteHandlerForRijmenamLogin : RouteHandlerBase
    {
        public RouteHandlerForRijmenamLogin(CaviaFactory caviaFactory) : base(caviaFactory)
        {
        }

        [Obsolete]
        public override bool GiveItToMe(HttpRequestArgs args, Dictionary<string, string> propertyBag)
        {
            if (args.GetHttpAction() != "POST") return false;
            var requestPath = args.GetRequestPath().Split(('/'));
            //if (requestPath.Count() == 3) return false;
            return requestPath[1].ToLower() == "login.html";
        }

        [Obsolete]
        public override void Handel(HttpRequestArgs args, CaviaCore caviaCore, Dictionary<string, string> propertyBag)
        {                       
            var userId = args.GetContentStringVariable("userid");
            var guid = args.GetContentStringVariable("guid");
          
            var ticket = new SecureTicket();
            ticket.Add("userId", userId);
            ticket.Add("guid", guid);

            var encoded = ticket.EncodeTicket();
            var urlEncoded = HttpUtility.UrlEncode(encoded);
                        
            args.Redirect(String.Format(@"./index.html?rijmenamUserProperties={0}",  urlEncoded));            
        }
    }
}