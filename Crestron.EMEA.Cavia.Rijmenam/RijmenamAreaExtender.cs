﻿using System;
using Crestron.EMEA.Cavia.Cavia.Core;
using Crestron.EMEA.Cavia.Cavia.SDK;

namespace Crestron.EMEA.Cavia.Rijmenam
{
    public class RijmenamAreaExtender : CaviaAreaExtender
    {  
        public string GetDescription(CaviaArea caviaArea)
        {
            return String.Format("({0})", caviaArea.Name);
        }
    }
}