﻿using Crestron.SimplSharp.CrestronIO;

namespace Crestron.EMEA.Cavia.Http
{
    class ResponseContextImpl : ResponseContext
    {
        private readonly HttpRequestArgs _args;

        public ResponseContextImpl(HttpRequestArgs args)
        {
            _args = args;
        }

        public void SendStream(Stream stream)
        {
            _args.SendStream(stream);
        }

        public void SendPageNotFound(string page)
        {
            _args.SendPageNotFound(page);
        }

        public void SendJson(object jsonObject)
        {
            _args.SendJson(jsonObject);
        }

        public void SendUnauthenticedResponse()
        {
            _args.SendUnauthenticedResponse();
        }
    }
}