﻿using System.Collections;
using Crestron.EMEA.I2P.Tools.IO;

namespace Crestron.EMEA.Cavia.Http.Cache
{
    public class FileStreamCache
    {
        private readonly StreamFactory _streamFactory;

        public FileStreamCache(StreamFactory streamFactory)
        {
            _streamFactory = streamFactory;
        }

        private readonly Hashtable _table = new Hashtable();

        public bool UseMemoryCache { get; set; }

        private readonly object _lockObject = new object();

        public Stream GetFile(string filename)
        {
            return GetFile(filename, UseMemoryCache);
        }

        public Stream GetFile(string filename, bool useCache)
        {
            Stream stream;
            if (useCache && _table.ContainsKey(filename))
            {                
                stream = (Stream)_table[filename];
            }
            else
            {
                lock (_lockObject)
                {

                    stream = GetFileFromDisk(filename);
                    if (useCache)
                    {
                        _table.Add(filename, stream);
                    }
                }
            }
            stream.Seek(0, SeekOrigin.Begin);
            return stream;
        }

        private MemoryStream GetFileFromDisk(string filename)
        {
            using (var fileStream = _streamFactory.CreateFileStream(filename))
            {
                var memStream = _streamFactory.CreateMemoryStream();
                CopyStream(fileStream, memStream);
                return memStream;
            }
        }

        public static void CopyStream(FileStream input, Stream output)
        {            
            var buffer = new byte[4096];
            int read;
            while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, read);
            }
            output.Seek(0, SeekOrigin.Begin);
        }       
    }
}