﻿using System;
using System.Collections.Generic;
using Crestron.EMEA.Cavia.Cavia.Core;

namespace Crestron.EMEA.Cavia.Http
{
    public class RequestContextImp : RequestContext
    {
        private readonly HttpRequestArgs _args;
        private readonly CaviaCore _caviaCore;

        private readonly Dictionary<string, string> _propertyBag = new Dictionary<string, string>();

        public RequestContextImp(HttpRequestArgs args, CaviaCore caviaCore)
        {
            _args = args;
            _caviaCore = caviaCore;
        }

        [Obsolete]
        public HttpRequestArgs HttpRequestArgs {
            get { return _args; }
        }

        [Obsolete]
        public Dictionary<string, string> PropertyBag {
            get { return _propertyBag; }
        }

        [Obsolete]
        public CaviaCore CaviaCore  {
            get { return _caviaCore; }
        }

        public void Add(string s, string value)
        {
            _propertyBag.Add(s, value);
        }

        public void SendPageNotFound(string msg)
        {
            _args.SendPageNotFound(msg);
        }

        public string GetQueryStringVariable(string key)
        {
            return _args.GetQueryStringVariable(key);
        }

        public string Get(string key)
        {
            return _propertyBag[key];
        }

        public string GetRequestPath()
        {
            return _args.GetRequestPath();
        }

        public string GetCaviaAuthenticationToken()
        {
            return _args.GetCaviaAuthenticationToken();
        }
    }
}