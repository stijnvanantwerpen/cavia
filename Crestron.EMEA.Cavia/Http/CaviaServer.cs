﻿namespace Crestron.EMEA.Cavia.Http
{
    public interface CaviaServer
    {        
        void Init(ushort port, string webFolder, string binFolder, string factoryName);
        void Start();
        void Stop();
        bool Running { get; }
    }
}
