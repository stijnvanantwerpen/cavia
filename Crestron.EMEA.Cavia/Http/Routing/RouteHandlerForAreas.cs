﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crestron.EMEA.Cavia.Cavia.Core;
using Crestron.EMEA.Cavia.Cavia.SDK;
using Crestron.EMEA.Cavia.Http.Tickets;
using Crestron.EMEA.Cavia.IoC;

namespace Crestron.EMEA.Cavia.Http.Routing
{
    public class RouteHandlerForAreas : RouteHandlerBase
    {
        private readonly CaviaAreaExtender _areaExtender;

        public RouteHandlerForAreas(CaviaFactory caviaFactory) : base(caviaFactory)
        {
            _areaExtender = caviaFactory.CreateAreaExtender();
        }   

        public override bool GiveItToMe(RequestContext requestContext)
        {
            var requestPath = requestContext.GetRequestPath().Split(('/'));
            return (requestPath[1].ToLower() == "areas" && requestPath.Count() == 2);
        }
      
        public override Action<ResponseContext> CreateResponse(RequestContext requestContext)
        {
            return responseContext =>
            {
                var ticketString = requestContext.GetCaviaAuthenticationToken();
                if (String.IsNullOrEmpty(ticketString))
                {
                    responseContext.SendUnauthenticedResponse();
                    return;
                }

                var ticket = new SecureTicket();
                ticket.LoadFromAuthenticationToken(ticketString);
                var isAdmin = ticket.Get("isAdmin") == "isAdmin";
                var filterByAreaNames = ticket.Get("filterByAreaNames") == "filter";
                var areaNames = ticket.Get("areaNames").Split(',');

                IEnumerable<CaviaArea> areas;
                if (isAdmin || !filterByAreaNames)
                {
                    areas = requestContext.CaviaCore.Areas;
                }
                else
                {
                    areas = requestContext.CaviaCore.Areas.Where(area => areaNames.Contains(area.Name));
                }

                var array = areas.Select(area =>
                {
                    area.SetDescription(_areaExtender.GetDescription(area));
                    return area;
                });                
                responseContext.SendJson(array);
            };        
        }
    }
}