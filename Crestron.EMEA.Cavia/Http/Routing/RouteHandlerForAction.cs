﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crestron.EMEA.Cavia.Cavia.Core;
using Crestron.EMEA.Cavia.IoC;
using Crestron.SimplSharp.Net;
using Newtonsoft.Json;

namespace Crestron.EMEA.Cavia.Http.Routing
{
    public class RouteHandlerForAction : RouteHandlerBase
    {
        public RouteHandlerForAction(CaviaFactory caviaFactory) : base(caviaFactory)
        {
        }
        
        [Obsolete]
        public override bool GiveItToMe(HttpRequestArgs args, Dictionary<string, string> propertyBag)
        {          
            if (args.GetHttpAction() != "POST") return false;
            var requestPath = args.GetRequestPath().Split(('/'));
            if( requestPath.Count()!= 5) return false;
            if (requestPath[1] != "areas") return false;
            return true;
        }

        [Obsolete]
        public override void Handel(HttpRequestArgs args, CaviaCore caviaCore, Dictionary<string, string> propertyBag)
        {
            var requestPath = args.GetRequestPath().Split(('/'));
            var areaName = HttpUtility.UrlDecode(requestPath[2]);
            var moduleName = HttpUtility.UrlDecode(requestPath[3]);
            var signal = HttpUtility.UrlDecode(requestPath[4]);
            var body = HttpUtility.UrlDecode(args.GetRequestBody());
            
            var valueObj = JsonConvert.DeserializeObject<CaviaAction>(body);

            caviaCore.DoAction(areaName, moduleName, signal, valueObj.Action);

            args.SendOk();
        }
    }
}