﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crestron.EMEA.Cavia.Cavia.Core;
using Crestron.EMEA.Cavia.IoC;

namespace Crestron.EMEA.Cavia.Http.Routing
{
    public class RouteHandlerForConfig : RouteHandlerBase
    {
        public RouteHandlerForConfig(CaviaFactory caviaFactory) : base(caviaFactory)
        {
        }

        [Obsolete]
        public override bool GiveItToMe(HttpRequestArgs args, Dictionary<string, string> propertyBag)
        {
            if (args.GetHttpAction() != "GET") return false;
            var requestPath = args.GetRequestPath().Split(('/'));
            if (requestPath.Count() != 2) return false;
            return requestPath[1] == "config";
        }

        [Obsolete]
        public override void Handel(HttpRequestArgs args, CaviaCore caviaCore, Dictionary<string, string> propertyBag)
        {
            var jsonConfig = new
            {
                factoryHello = caviaCore.CaviaFactory.GetHello(),
                factoryName = caviaCore.CaviaFactory.GetType().FullName,
                running = caviaCore.CaviaServer.Running
            };           
            args.SendJson(jsonConfig);
        }
    }
}