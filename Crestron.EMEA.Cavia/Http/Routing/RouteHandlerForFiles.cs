﻿using System;
using Crestron.EMEA.Cavia.Http.Cache;
using Crestron.EMEA.Cavia.IoC;
using Crestron.EMEA.I2P.Tools.SSharp.IO;
using Crestron.SimplSharp.CrestronIO;
using Crestron.SimplSharp.Net;

namespace Crestron.EMEA.Cavia.Http.Routing
{
    public class RouteHandlerForFiles : RouteHandlerBase
    {
        private readonly FileStreamCache _cache;

        public RouteHandlerForFiles(CaviaFactory caviaFactory)
            : base(caviaFactory)
        {
            _cache = caviaFactory.CreateFileStreamCache();
            //_cache.UseMemoryCache = true;
            _cache.UseMemoryCache = false;
        }

        public override bool GiveItToMe(RequestContext requestContext)
        {
            var path = GetPath(requestContext);
            return File.Exists(path);            
        }

        public override Action<ResponseContext> CreateResponse(RequestContext requestContext)
        {
            return (response) =>
            {
                var useCache = requestContext.GetQueryStringVariable("noCache") != "true";
                var path = GetPath(requestContext);
                var stream = _cache.GetFile(path, useCache);
                
                response.SendStream(stream.ToCrestronStream());
            };
        }

        protected string GetPath(RequestContext requestContext)
        {
            var webFolder = requestContext.Get("webFolder");
            var requestpath = requestContext.GetRequestPath();
            if(requestpath.StartsWith(@"/")){
                requestpath = requestpath.Remove(0,1);
            }
            var path = HttpUtility.UrlDecode(String.Format(@"{0}\{1}", webFolder, requestpath));
            if(path.StartsWith(@"\")){
                path = path.Remove(0,1);
            }
            return path;
        }   
    }
}