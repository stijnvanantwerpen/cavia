﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Crestron.EMEA.Cavia.Cavia.Core;
using Crestron.EMEA.Cavia.Cavia.SDK;
using Crestron.EMEA.Cavia.DataLayer;
using Crestron.EMEA.Cavia.Http.Tickets;
using Crestron.EMEA.Cavia.IoC;

namespace Crestron.EMEA.Cavia.Http.Routing
{
    public class RouteHandlerForUserAuthentication : RouteHandlerBase
    {
        readonly CaviaAuthenticationProvider _caviaAuthenticationProvider;

        public RouteHandlerForUserAuthentication(CaviaFactory caviaFactory):base(caviaFactory)
        {
            _caviaAuthenticationProvider = caviaFactory.CreateAuthenticationProvider();        
        }

        [Obsolete]
        public override bool GiveItToMe(HttpRequestArgs args, Dictionary<string, string> propertyBag)
        {
            if (args.GetHttpAction() != "GET") return false;
            var requestPath = args.GetRequestPath().Split(('/'));
            if (requestPath.Count() != 2) return false;
            return requestPath[1] == "login";
        }

        [Obsolete]
        public override void Handel(HttpRequestArgs args, CaviaCore caviaCore, Dictionary<string, string> propertyBag)
        {
            var user = _caviaAuthenticationProvider.CanAuthenticate(args) ? 
                _caviaAuthenticationProvider.Authenticate(args): new UserDTO
                {
                    UserId = 0,
                    AreaNames = "",
                    FilterByAreaNames = false,
                    IsAdmin = false,
                    IsGuest = true,
                    UserAllowed = false
                } ;
            var ticket = new SecureTicket();
            ticket.Add("userId", user.UserId.ToString(CultureInfo.InvariantCulture));
            ticket.Add("isAdmin", user.IsAdmin ? "isAdmin" : "isNoAdmin");
            ticket.Add("isGuest", user.IsGuest ? "isGuest" : "isNoGuest");
            ticket.Add("filterByAreaNames", user.FilterByAreaNames ? "filter": "noFilter");
            ticket.Add("areaNames", user.AreaNames);
            ticket.Add("validFrom", DateTime.Now.ToString("yyyyMMddhhmmss"));
            ticket.Add("validTill", DateTime.Now.AddMinutes(30).ToString("yyyyMMddhhmmss"));
            ticket.Add("authenticatedBy", GetAuthenticatorName());
            var decodedTicket = ticket.EncodeTicket();
            args.SendJsonString(("{" + String.Format(" 'ticket': '{0}' ", decodedTicket) + "}").Replace("'", "\""));
        }

        private string GetAuthenticatorName()
        {
            return String.Format("{0}.{1}", _caviaAuthenticationProvider.GetType().Namespace, _caviaAuthenticationProvider.GetType().Name);
        }
    }
}