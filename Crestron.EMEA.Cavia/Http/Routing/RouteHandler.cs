﻿using System;

namespace Crestron.EMEA.Cavia.Http.Routing
{    
    public interface RouteHandler
    {
        Action<ResponseContext> CreateResponse(RequestContext requestContext);
        bool GiveItToMe(RequestContext requestContext);
    }
}