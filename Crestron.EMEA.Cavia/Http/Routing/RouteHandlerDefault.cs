﻿using System;
using Crestron.EMEA.Cavia.IoC;

namespace Crestron.EMEA.Cavia.Http.Routing
{
    public class RouteHandlerDefault : RouteHandlerBase
    {
        public RouteHandlerDefault(CaviaFactory caviaFactory)
            : base(caviaFactory)
        {
        }

        public bool GiveItToMe(RequestContextImp context)
        {
           return true;
        }
            

        public override Action<ResponseContext> CreateResponse(RequestContext requestContext)
        {
            return (responseContext) => responseContext.SendPageNotFound("page not found");
        }        
    }
}