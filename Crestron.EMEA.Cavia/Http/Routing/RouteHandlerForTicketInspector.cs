﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.EMEA.Cavia.Cavia.Core;
using Crestron.EMEA.Cavia.Http.Tickets;
using Crestron.EMEA.Cavia.IoC;

namespace Crestron.EMEA.Cavia.Http.Routing
{
    public class RouteHandlerForTicketInspector : RouteHandlerBase
    {
        public RouteHandlerForTicketInspector(CaviaFactory caviaFactory) : base(caviaFactory)
        {
        }

        [Obsolete]
        public override bool GiveItToMe(HttpRequestArgs args, Dictionary<string, string> propertyBag)
        {
            if (args.GetHttpAction() != "GET") return false;
            var requestPath = args.GetRequestPath().Split(('/'));
            if (requestPath.Count() != 2) return false;
            return requestPath[1] == "ticket";
        }

        [Obsolete]
        public override void Handel(HttpRequestArgs args, CaviaCore caviaCore, Dictionary<string, string> propertyBag)
        {
            var authenticationToken = args.GetCaviaAuthenticationToken();
            if (String.IsNullOrEmpty(authenticationToken))
            {
                args.SendJsonString("{ 'msg': 'no cavia authentication token found!'}".Replace("'","\""));
                return;
            }

            var ticket = new SecureTicket();            
            ticket.LoadFromAuthenticationToken(authenticationToken);
            var sb = new StringBuilder();
            sb.Append("{");
            sb.Append(String.Join(", ", 
                ticket.Keys.Select(key => 
                    String.Format("'{0}': '{1}'", key, ticket.Get(key))).ToArray()));                                        
            sb.Append("}");
            args.SendJsonString(sb.ToString().Replace("'", "\""));
        }
    }
}