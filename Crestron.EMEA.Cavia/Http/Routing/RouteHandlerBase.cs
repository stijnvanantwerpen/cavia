﻿using System;
using System.Collections.Generic;
using Crestron.EMEA.Cavia.Cavia.Core;
using Crestron.EMEA.Cavia.IoC;

namespace Crestron.EMEA.Cavia.Http.Routing
{
    public abstract class RouteHandlerBase : RouteHandler
    {
        protected CaviaFactory CaviaFactory { get; private set; }

        protected RouteHandlerBase(CaviaFactory caviaFactory)
        {
            CaviaFactory = caviaFactory;
        }

        [Obsolete]
        public virtual bool GiveItToMe(HttpRequestArgs args, Dictionary<string, string> propertyBag)
        {
            throw new NotImplementedException();
        }

        [Obsolete]
        public virtual void Handel(HttpRequestArgs args, CaviaCore caviaCore, Dictionary<string, string> propertyBag)
        {
            throw new NotImplementedException();
        }

        public virtual bool GiveItToMe(RequestContext requestContext)
        {          
// ReSharper disable CSharpWarnings::CS0612
            return GiveItToMe(((RequestContextImp)requestContext).HttpRequestArgs, ((RequestContextImp)requestContext).PropertyBag);
// ReSharper restore CSharpWarnings::CS0612
        }

        public virtual Action<ResponseContext> CreateResponse(RequestContext requestContext)
        {
// ReSharper disable once CSharpWarnings::CS0612
            return (responseContext) => Handel((RequestContextImp)requestContext);
        }

        [Obsolete]
        public virtual void Handel(RequestContextImp requestContext)
        {

// ReSharper disable CSharpWarnings::CS0612
            Handel(requestContext.HttpRequestArgs, requestContext.CaviaCore, requestContext.PropertyBag);
// ReSharper restore CSharpWarnings::CS0612
        }     
    }
}