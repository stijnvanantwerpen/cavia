﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crestron.EMEA.Cavia.Cavia.Core;
using Crestron.EMEA.Cavia.IoC;

namespace Crestron.EMEA.Cavia.Http.Routing
{
    public class RouteHandlerForArea : RouteHandlerBase
    {
        public RouteHandlerForArea(CaviaFactory caviaFactory) : base(caviaFactory)
        {
        }

        [Obsolete]
        public override bool GiveItToMe(HttpRequestArgs args, Dictionary<string, string> propertyBag)
        {            
            if(args.GetHttpAction() != "GET") return false;
            var requestPath = args.GetRequestPath().Split(('/'));
            if(requestPath.Count() == 3) return false;
            return requestPath[1].ToLower() == "areas";
        }

        [Obsolete]
        public override void Handel(HttpRequestArgs args, CaviaCore caviaCore, Dictionary<string, string> propertyBag)
        {
            var requestPath = args.GetRequestPath().Split(('/'));
            var areaName = requestPath[2];
            
            var area = caviaCore.Areas.SingleOrDefault(a => a.Name == areaName);
            if (area == null)
            {
                args.SendPageNotFound(String.Format("No area with Name '{0}' was found.", areaName));
            }
            else
            {
                args.SendJson(area);
            }
        }
    }
}