﻿using Crestron.EMEA.Cavia.Cavia.Core;

namespace Crestron.EMEA.Cavia.Http
{
    public interface RequestContext
    {
        void Add(string s, string value);
        void SendPageNotFound(string msg);
        string GetQueryStringVariable(string key);
        string Get(string webfolder);
        string GetRequestPath();
        string GetCaviaAuthenticationToken();
        CaviaCore CaviaCore { get; }
    }
}