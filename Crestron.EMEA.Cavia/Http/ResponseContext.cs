﻿using Crestron.SimplSharp.CrestronIO;

namespace Crestron.EMEA.Cavia.Http
{
    public interface ResponseContext
    {
        void SendStream(Stream stream);
        void SendPageNotFound(string pageNotFound);
        void SendJson(object jsonObject);
        void SendUnauthenticedResponse();
    }
}