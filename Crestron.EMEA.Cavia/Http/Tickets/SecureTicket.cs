﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Crestron.EMEA.Cavia.Http.Tickets
{
    // It has to be noted that Base64 encoding is no way considerd as secure encription.
    // The intend of this class however is not to add security, but to put a mechanisme
    // in place to prepare for it. By replacing the Base64 encoding with Private/Public 
    // key encoding, real security can be implemented.
    // The main reason to use Base64 encoded strings is to confuscate the ticket to prevent 
    // manipulation of the ticket in the frontend application.
    public class SecureTicket
    {
        private readonly Dictionary<string, string> _dictionary = new Dictionary<string, string>();
        public IEnumerable<string> Keys {
            get { return _dictionary.Keys; }
        }

        public void Add(string key, string value)
        {
            _dictionary.Add(EncodeAsBase64(key), EncodeAsBase64(value));
        }

        public string Get(string key)
        {
            if (!_dictionary.ContainsKey(key)) return String.Empty;

            return _dictionary[key];
        }

        public string EncodeTicket()
        {
            var ticketString = String.Join(";", _dictionary.Select((k,i) => String.Format("{0}:{1}", k.Key, k.Value)).ToArray());
            return EncodeAsBase64(ticketString);
        }

        public void LoadFromAuthenticationToken(string ticket)
        {
            if (String.IsNullOrEmpty(ticket)) return;

            var ticketString = DecodeFromBase64(ticket);
            foreach(var part in ticketString.Split(';'))
            {
                var pair = part.Split(':');
                _dictionary.Add(DecodeFromBase64(pair[0]),DecodeFromBase64(pair[1]));
            }            
        }

        private string EncodeAsBase64(string original)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(original));
        }

        private string DecodeFromBase64(string base64)
        {
            if (String.IsNullOrEmpty(base64)) return String.Empty;

            var bytes = Convert.FromBase64String(base64);
            var result = Encoding.UTF8.GetString(bytes, 0, bytes.Count());
            return result;
        }
    }
}