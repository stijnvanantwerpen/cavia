﻿using System;

namespace Crestron.EMEA.Cavia.Http.Filters
{
    public class RequestFilterForNoCache : RequestFilter
    {
        
        public Action Filter(RequestContextImp requestContext)
        {
            return () =>
            {
                var noCache = requestContext.GetQueryStringVariable("noCache");
                var useCached = noCache != "true" ? "useCache" : "doNotUseCache";
                requestContext.Add("useCache", useCached);
            };
        }        
    }
}