﻿using System;

namespace Crestron.EMEA.Cavia.Http.Filters
{
    public interface RequestFilter
    {
        Action Filter(RequestContextImp requestContext);
    }
}