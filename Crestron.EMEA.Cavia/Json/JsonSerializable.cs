﻿namespace Crestron.EMEA.Cavia.Core.Json
{
    public interface JsonSerializable
    {
        string ToJson();
        void Load(string jsonString);
    }
}