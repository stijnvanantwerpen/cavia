﻿namespace Crestron.EMEA.Cavia.Core.Json
{   
    public class JsonSerializerImp
    {
        public string Serialize(JsonSerializable obj)
        {
            return obj.ToJson();
        }

        internal T DeSerialize<T>(T objToLoad, string body) where T : JsonSerializable, new()
        {            
            objToLoad.Load(body);
            return objToLoad;
        }
    }
}