﻿using System;

namespace Crestron.EMEA.Cavia
{   
    public class CaviaException : ApplicationException
    {
        public CaviaException(string msg):base(msg)
        {            
        }
    }
}