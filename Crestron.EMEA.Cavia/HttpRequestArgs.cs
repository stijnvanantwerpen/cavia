﻿using Crestron.SimplSharp.CrestronIO;

namespace Crestron.EMEA.Cavia
{
    public interface HttpRequestArgs
    {
        void HelloWorldResponse();

        string GetRequestPath();

        void HelloWorldResponse(string p);
        void SendJson(object caviaModule);

        void SendUnhandableRequestError();

        void SendPageNotFound(string msg);
        void SendJsonString(string json);

        string GetHttpAction();
        string GetHeaderValueFor(string headerName);

        void SendOk();

        string GetRequestBody();
        void SendStream(Stream fileStream);

        string GetContentStringVariable(string queryVar);
        string GetQueryStringVariable(string queryVar);
        

        void SendExceptionOccured(System.Exception ex);

        string GetCaviaAuthenticationToken();

        void Redirect(string url);
        void Redirect(string url, int code);
        void SendUnauthenticedResponse();

        string GetResponseDescription();

        string GetRequestorHost();
    }
}