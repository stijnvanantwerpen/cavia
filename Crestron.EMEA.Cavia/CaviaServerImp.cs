﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crestron.EMEA.Cavia.Cavia.Core;
using Crestron.EMEA.Cavia.Http;
using Crestron.EMEA.Cavia.Http.Filters;
using Crestron.EMEA.Cavia.Http.Routing;
using Crestron.EMEA.Cavia.IoC;

// ReSharper disable once CheckNamespace
// Since Server (instance off CaviaServer) is a public property on CaviaSimplePlusFacade it needs 
// to be in the same namespace. If not S+ compiler does not find it.
namespace Crestron.EMEA.Cavia
{
    public class CaviaServerImp : CaviaServer
    {
     
        private CaviaFactory _caviaFactory;
        private string _webFolder;
        private bool _isInitialized;
        private HttpServer _webServer;
        private readonly CaviaCore _caviaCore;
        private readonly CaviaFactoryProvider _caviaFactoryProvider;

        private readonly List<RouteHandler> _routeHandlers = new List<RouteHandler>();
        private readonly List<RequestFilter> _requestFilters = new List<RequestFilter>();

        private string _factoryName;
        private string _binFolder;
        private ushort _port;

        public CaviaServerImp()
        {
            //Only here for compatibility with S+
            throw new NotImplementedException("Not intended to use.");
        }

        public CaviaServerImp(CaviaCore caviaCore, CaviaFactoryProvider caviaFactoryProvider)
        {            
            _caviaCore = caviaCore;
            _caviaCore.SetServer(this);
            _caviaFactoryProvider = caviaFactoryProvider;            
        }

        public void Init(ushort port, string webFolder,string binFolder, string factoryName)
        {
            if (_isInitialized) throw new CaviaException("Cavia Server already initialized!");
            _isInitialized = true;
            _webFolder = webFolder;
            _binFolder = binFolder;
            _factoryName = factoryName;
            _port = port;

            LoadFactory();
            LoadWebServer();
        }        

        private void LoadFactory()
        {
            _caviaFactory = _caviaFactoryProvider.CreateFactory(_binFolder, _factoryName);
            _caviaCore.SetFactory(_caviaFactory);
        }

        private void LoadWebServer()
        {
            _webServer = _caviaFactory.CreateHttpServer();
            _webServer.OnHttpRequest += OnHttpRequestHandler;
            _webServer.Port = _port;
        }

        private void LoadRouteHandlers()
        {
            _routeHandlers.RemoveRange(0, _routeHandlers.Count);
            foreach (var rh in _caviaFactory.CreateRouteHandlers())
            {
                _routeHandlers.Add(rh);
            }
        }

        private void LoadRequestFilters()
        {
            _requestFilters.RemoveRange(0, _requestFilters.Count);
            foreach (var rh in _caviaFactory.CreateRequestFilters())
            {
                _requestFilters.Add(rh);
            }
        }

        public void Start()
        {            
            InitalizationGaurd();
            if (Running) return;

            LoadRequestFilters();
            LoadRouteHandlers();
                                    
            _webServer.Start();
            Running = true;
        }

        public void Stop()
        {
            InitalizationGaurd();
            _webServer.Stop();
            Running = false;
        }

        public bool Running { get; private set; }

        private void OnHttpRequestHandler(HttpRequestArgs httpRequestArgs)
        {            
            var requestContext = new RequestContextImp(httpRequestArgs, _caviaCore);
            requestContext.Add("webFolder", _webFolder);

            try
            {
                var firstHandler =_routeHandlers.First(h => h.GiveItToMe(requestContext));                             
                firstHandler.CreateResponse(requestContext).Invoke(new ResponseContextImpl(httpRequestArgs) );
            }
            catch (Exception ex)
            {
                httpRequestArgs.SendExceptionOccured(ex);
            }
        }

        private void InitalizationGaurd()
        {
            if (!_isInitialized) throw new CaviaException("Not yet Initialized");
        }
    }
}