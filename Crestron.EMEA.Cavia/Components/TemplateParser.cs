﻿using System.Text;

namespace Crestron.EMEA.Cavia.Components
{
    public class TemplateParser
    {
        private const string Prefix = "%:";
        private const string Sufix = ":";

        readonly StringBuilder _sb = new StringBuilder();

        public string Parse()
        {
            return _sb.ToString();
        }

        public void LoadTemplate(string template)
        {
            _sb.Append(template);
        }

        public void Set(string key, string value)
        {
            _sb.Replace(Prefix+key+Sufix, value);
        }
    }
}