﻿namespace Crestron.EMEA.Cavia.DataLayer
{
    public class UserDTO
    {
        //TODO: Some key-value list
        //public DateTime LastAction { get; set; }
        //TODO: List of Area's and "All"
        public bool FilterByAreaNames { get; set; }
        public string AreaNames { get; set; }
        public bool UserAllowed { get; set; }
        public bool IsAdmin { get; set; }
        public uint UserId { get; set; }
        public bool IsGuest { get; set; }        
    }
}