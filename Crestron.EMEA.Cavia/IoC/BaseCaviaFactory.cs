﻿using System.Collections.Generic;
using Crestron.EMEA.Cavia.Cavia.Core;
using Crestron.EMEA.Cavia.Cavia.SDK;
using Crestron.EMEA.Cavia.Http.Cache;
using Crestron.EMEA.Cavia.Http.Filters;
using Crestron.EMEA.Cavia.Http.Routing;
using Crestron.EMEA.Cavia.IoC.CrestronWrappers;
using Crestron.EMEA.I2P.Tools.SSharp.IO;


namespace Crestron.EMEA.Cavia.IoC
{
    public abstract class BaseCaviaFactory : CaviaFactory
    {
        public HttpServer CreateHttpServer()
        {
            return new HttpServerWrapper();
        }

        public abstract CaviaAuthenticationProvider CreateAuthenticationProvider();

        public abstract string GetHello();

        public virtual IEnumerable<RouteHandler> CreateRouteHandlers()
        {
            yield return new RouteHandlerForTicketInspector(this);
            yield return new RouteHandlerForUserAuthentication(this);
            yield return new RouteHandlerForAction(this);
            yield return new RouteHandlerForAreas(this);
            yield return new RouteHandlerForArea(this);
            yield return new RouteHandlerForFiles(this);
            yield return new RouteHandlerForConfig(this);
            yield return new RouteHandlerDefault(this);
        }

        public virtual IEnumerable<RequestFilter> CreateRequestFilters()
        {
            yield return new RequestFilterForNoCache();
        }

        public FileStreamCache CreateFileStreamCache()
        {
            return new FileStreamCache(new StreamFactoryForCrestron());
        }

        public virtual CaviaAreaExtender CreateAreaExtender()
        {
            return new CaviaAreaExtenderImpl();
        }
    }
}