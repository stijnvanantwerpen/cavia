﻿using System;

namespace Crestron.EMEA.Cavia.IoC.CrestronWrappers
{
    public class HttpServerWrapper : HttpServer
    {
        readonly SimplSharp.Net.Http.HttpServer _httpServer = new SimplSharp.Net.Http.HttpServer();

        public event Action<HttpRequestArgs> OnHttpRequest
        {
            add { _httpServer.OnHttpRequest += (sender, e) => value(new HttpRequestArgsWrapper(e)); }
            remove { throw new NotImplementedException(); }
        }

        public HttpServerWrapper()
        {
            _httpServer.ServerName = "Crestron's CaviaSimplePlusFacade Server";            
        }

        public void Start()
        {
            _httpServer.Open();
        }

        public void Stop()
        {
            _httpServer.Close();
        }

        public ushort Port {
            get { return (ushort)_httpServer.Port; }
            set { _httpServer.Port = value; }
        }

    }
}