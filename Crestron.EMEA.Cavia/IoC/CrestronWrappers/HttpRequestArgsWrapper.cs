﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crestron.SimplSharp.CrestronIO;
using Crestron.SimplSharp.Net;
using Crestron.SimplSharp.Net.Http;
using Newtonsoft.Json;

namespace Crestron.EMEA.Cavia.IoC.CrestronWrappers
{
    public class HttpRequestArgsWrapper : HttpRequestArgs
    {
        private readonly SimplSharp.Net.Http.OnHttpRequestArgs _inner;
        public HttpRequestArgsWrapper(SimplSharp.Net.Http.OnHttpRequestArgs e)
        {
            _inner = e;            
        }

        public void HelloWorldResponse()
        {
            HelloWorldResponse("World");
        }

        public string GetRequestPath()
        {
            return _inner.Request.Path;
        }

        public void HelloWorldResponse(string p)
        {
            _inner.Response.ContentString = String.Format("Hello {0}!", p);
        }

        public void SendJson(Object obj)
        {
            var jsonString = JsonConvert.SerializeObject(obj); 
            SendJsonString(jsonString);
        }

        public void SendUnhandableRequestError()
        {            
            _inner.Response.SendError(550, "Sorry, Cavia did not know how to handle this request");
        }

        public void SendPageNotFound(string msg)
        {            
            //_inner.Response.SendError(404, msg);
            var bodyHtml = @"<h1>Hello 404</h1>";
            _inner.Response.SendErrorWithCustomBody(404, msg, bodyHtml);
        }

       
        public void SendJsonString(string jsonString)
        {                      
            _inner.Response.Header.AddHeader(new HttpHeader("Content-Type", "application/json"));
            _inner.Response.ContentString = jsonString;
        }


        public string GetHttpAction()
        {
            return _inner.Request.Header.RequestType;
        }

        public void SendOk()
        {
            _inner.Response.Code = 200;
            _inner.Response.ContentString = String.Empty;
        }

        public string GetRequestBody()
        {
            return _inner.Request.HasContentLength ? 
                _inner.Request.ContentString : String.Empty;
        }

        public void SendStream(Stream stream)
        {            
            _inner.Response.ContentSource = ContentSource.ContentStream;         
            _inner.Response.CloseStream = false;                                    
            _inner.Response.ContentStream = stream;
        }     

        public string GetContentStringVariable(string queryVar)
        {
            var result = GetVariableHelper(GetRequestBody(), queryVar);            
            return result;            
        }

        public string GetQueryStringVariable(string queryVar)
        {
            return _inner.Request.QueryString.ToString().Split('&')
                .Select(part => part.Split('='))
                .Where(pair => pair[0] == queryVar)
                .Select(pair => HttpUtility.UrlDecode(pair[1]))
                .DefaultIfEmpty(String.Empty)
                .Single();
        }

        public void SendExceptionOccured(Exception ex)
        {
            _inner.Response.Code = 551;
            _inner.Response.ResponseText = "Cavia Exception";
            _inner.Response.ContentString = String.Format(
                "Sorry, Cavia encountered an exception. Details: {0}. \n\n {1}",
                ex.Message, ex.StackTrace);            
        }

        public string GetCaviaAuthenticationToken()
        {
            var result = _inner.Request.Header.GetHeaderValue("caviaAuthenticationToken");
            result = result ?? GetQueryStringVariable("caviaAuthenticationToken");
            return result ?? String.Empty;
        }

        private Dictionary<string, string> _cachCollection;
                   
        private string GetVariableHelper(string contentString, string queryVar)
        {
            if (String.IsNullOrEmpty(contentString)) return String.Empty;
            if (String.IsNullOrEmpty(queryVar)) return String.Empty;
            if (_cachCollection != null && !_cachCollection.ContainsKey(queryVar)) return String.Empty;
            if (_cachCollection != null) return _cachCollection[queryVar];

            _cachCollection = new Dictionary<string, string>();
            foreach (var pair in contentString.Split('&').Where(part => part.Contains('=')).Select(part => part.Split('=')).Where(pair => pair.Length == 2))
            {
                _cachCollection.Add(pair[0], pair[1]);
            }

            return _cachCollection[queryVar];            
        }

     
        public void Redirect(string url)
        {
            Redirect(url, 302);
        }

        public void Redirect(string url, int code)
        {
            _inner.Response.Code = code;
            _inner.Response.Header.SetHeaderValue("Location", url);
        }

        public void SendUnauthenticedResponse()
        {
            _inner.Response.Code = 401;
            _inner.Response.ContentString = "No Cavia Authentication Ticket was provided.";
        }

        public string GetResponseDescription()
        {
            return String.Format("[{0}] {1}", _inner.Response.Code, _inner.Response.ResponseText);
        }

        public string GetRequestorHost()
        {
            return _inner.Request.Header["Host"].Value;
        }

        public string GetHeaderValueFor(string headerName)
        {
            return _inner.Request.Header.GetHeaderValue(headerName);
        }       
    }
}