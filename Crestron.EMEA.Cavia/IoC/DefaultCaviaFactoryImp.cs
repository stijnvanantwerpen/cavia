﻿using Crestron.EMEA.Cavia.Cavia.Authentication;
using Crestron.EMEA.Cavia.Cavia.SDK;

namespace Crestron.EMEA.Cavia.IoC
{
    public class DefaultCaviaFactoryImp : BaseCaviaFactory
    {
        public override CaviaAuthenticationProvider CreateAuthenticationProvider()
        {
            return new DefaultAuthenticationProvider();
        }

        public override string GetHello()
        {
            return "Hello, this is the Default Factory";
        }
    }
}