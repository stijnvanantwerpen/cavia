﻿using System;
using System.Linq;
using Crestron.SimplSharp;
using Crestron.SimplSharp.Reflection;

namespace Crestron.EMEA.Cavia.IoC
{
    public class CaviaFactoryProviderForCrestron : CaviaFactoryProvider
    {
        public CaviaFactory CreateFactory(string binFolder, string factoryAssemblyName)
        {
            if (String.IsNullOrEmpty(factoryAssemblyName) || String.IsNullOrEmpty(binFolder))
            {
                return new DefaultCaviaFactoryImp();
            }

            try
            {
                var path = String.Format(@"{0}\{1}.dll", binFolder, factoryAssemblyName);
                CrestronConsole.Print(String.Format("Loading Custom Factory from '{0}'.", path));
                var assembly = Assembly.LoadFrom(path);
                var factoryType = assembly.GetTypes().Single(t => typeof(CaviaFactory).IsAssignableFrom(t));
                var emptyTypes = new CType[0];
                var emptyArgs = new object[0];
                var factory = factoryType.GetConstructor(emptyTypes).Invoke(emptyArgs) as CaviaFactory;
                return factory;
            }catch(Exception ex)
            {
                // When we fail to load the custom factory, we return the default factory
                // So, the server will be usable.
                // The user shall be able to visit the config page and observe that his custom factory
                // is not loaded.
                CrestronConsole.Print(String.Format("Loading Custom Factory failed '{0}'.", ex.Message));
                return new DefaultCaviaFactoryImp();
            }
        }
    }
}