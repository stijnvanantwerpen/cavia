﻿using System.Collections.Generic;
using Crestron.EMEA.Cavia.Cavia.SDK;
using Crestron.EMEA.Cavia.Http.Filters;
using Crestron.EMEA.Cavia.Http.Routing;

namespace Crestron.EMEA.Cavia.IoC
{
    public interface CaviaFactory
    {
        //HttpServer CreateHttpServer(Action<HttpRequestArgs> onHttpRequestHandler);
        HttpServer CreateHttpServer();

        CaviaAuthenticationProvider CreateAuthenticationProvider();

        string GetHello();

        IEnumerable<RouteHandler> CreateRouteHandlers();

        IEnumerable<RequestFilter> CreateRequestFilters();

        Http.Cache.FileStreamCache CreateFileStreamCache();
        
        CaviaAreaExtender CreateAreaExtender();
    }
}