﻿namespace Crestron.EMEA.Cavia.IoC
{
    public interface CaviaFactoryProvider
    {
        CaviaFactory CreateFactory(string binFolder, string factoryAssemblyName);
    }
}