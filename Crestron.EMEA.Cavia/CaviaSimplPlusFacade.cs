﻿using System;
using Crestron.EMEA.Cavia.Cavia.Core;
using Crestron.EMEA.Cavia.Cavia.Core.Imp;
using Crestron.EMEA.Cavia.IoC;


namespace Crestron.EMEA.Cavia
{
    public class CaviaSimplPlusFacade
    {
        private static readonly CaviaCore CaviaCore = new CaviaCoreImp();

        private static readonly CaviaFactoryProvider CaviaFactoryProvider = new CaviaFactoryProviderForCrestron();

        private static CaviaServerImp _caviaServerInstance;

        private static readonly Object ThisLock = new Object();

        public CaviaServerImp Server
        {
            get
            {
                lock (ThisLock)
                {
                    if (_caviaServerInstance == null)
                    {
                        _caviaServerInstance = new CaviaServerImp(CaviaCore, CaviaFactoryProvider);
                    }
                }
                return _caviaServerInstance;
            }
        }

        public void RegisterCaviaModule(CaviaModuleImp caviaModule)
        {
            CaviaCore.RegisterModule(caviaModule);
        }
    }
}