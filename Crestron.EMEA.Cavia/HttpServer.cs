﻿using System;

namespace Crestron.EMEA.Cavia
{
    public interface HttpServer
    {
        void Start();

        void Stop();

        ushort Port { get; set; }

        event Action<HttpRequestArgs> OnHttpRequest;
    }
}