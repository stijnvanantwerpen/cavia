﻿using System.Collections.Generic;
using Crestron.EMEA.Cavia.Http;
using Crestron.EMEA.Cavia.IoC;

namespace Crestron.EMEA.Cavia.Cavia.Core
{
    public interface CaviaCore
    {
        IEnumerable<CaviaArea> Areas { get; }
        CaviaFactory CaviaFactory { get; }
        CaviaServer CaviaServer { get; }

        void RegisterModule(CaviaModule caviaModule);

        void DoAction(string areaName, string moduleName, string action, string value);

        void SetFactory(CaviaFactory caviaFactory);

        void SetServer(CaviaServer caviaServer);
    }
}