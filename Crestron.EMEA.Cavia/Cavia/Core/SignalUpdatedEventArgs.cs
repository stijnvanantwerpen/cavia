﻿using System;

namespace Crestron.EMEA.Cavia.Cavia.Core
{
    public class SignalUpdatedEventArgs : EventArgs
    {
        public string SignalType { get; set; }
        public ushort SignalIndex { get; set; }
        public string SignalName { get; set; }
        public string Action { get; set; }
    }
}