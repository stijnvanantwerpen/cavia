﻿namespace Crestron.EMEA.Cavia.Cavia.Core
{
    public interface CaviaModule
    {
        string AreaName { get; set; }
        string ModuleName { get; set; }
        string ModuleType { get; set; }

        void RegisterSignal(string type, ushort index, string signalName);

        void DoAction(string signalName, string action);

        void SetValue(string outputName, string value);
    }
}