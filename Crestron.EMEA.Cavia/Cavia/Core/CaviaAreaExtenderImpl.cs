using Crestron.EMEA.Cavia.Cavia.SDK;

namespace Crestron.EMEA.Cavia.Cavia.Core
{
    class CaviaAreaExtenderImpl : CaviaAreaExtender
    {        
        public string GetDescription(CaviaArea caviaArea)
        {
            return caviaArea.Name;
        }
    }
}