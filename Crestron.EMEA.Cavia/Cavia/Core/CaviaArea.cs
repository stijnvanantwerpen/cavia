﻿using System.Collections.Generic;

namespace Crestron.EMEA.Cavia.Cavia.Core
{
    public interface CaviaArea 
    {
        string Name { get; }
        string Description { get; }

        IEnumerable<CaviaModule> CaviaModules { get; }

        void SetDescription(string description);
    }
}