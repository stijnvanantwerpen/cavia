﻿using System;

namespace Crestron.EMEA.Cavia
{
    public class SignalUpdatedArgs : EventArgs
    {
        public string SignalType { get; set; }
        public ushort SignalIndex { get; set; }
        public string SignalName { get; set; }
        public string Action { get; set; }
    }
}