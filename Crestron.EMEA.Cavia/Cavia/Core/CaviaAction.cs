﻿using Newtonsoft.Json;

namespace Crestron.EMEA.Cavia.Cavia.Core
{
    public class CaviaAction
    {
        [JsonProperty(PropertyName = "action")]        
        public string Action { get; set; }     
    }
}