﻿using Newtonsoft.Json;

namespace Crestron.EMEA.Cavia.Cavia.Core.Imp
{
    public class Signal
    {
        [JsonProperty(PropertyName = "signalName")]
        public string SignalName { get; set; }
        
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }
        
        [JsonProperty(PropertyName = "index")]
        public ushort Index { get; set; }
        
        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }        
    }
}