﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Crestron.EMEA.Cavia.Cavia.Core.Imp
{
    public class CaviaAreaImp : CaviaArea
    {
        [JsonProperty(PropertyName = "areaName")]
        public string Name { get; private set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; private set; }

        [JsonProperty(PropertyName = "modules")]
        public IEnumerable<CaviaModule> CaviaModules { get; private set; }

        
        public CaviaAreaImp(string name, IEnumerable<CaviaModule> caviaModules)
        {
            Name = name;
            CaviaModules = caviaModules;
        }

        public void SetDescription(string description)
        {
            Description = description;
        }
    }
}