﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Crestron.EMEA.Cavia.Cavia.Core.Imp
{
    public class CaviaModuleImp : CaviaModule
    {
        private readonly List<Signal> _signals = new List<Signal>();

        public event EventHandler<SignalUpdatedEventArgs> OnSignalUpdated;

        [JsonProperty(PropertyName = "area")]
        public String AreaName { get; set; }
        
        [JsonProperty(PropertyName = "moduleName")]        
        public String ModuleName { get; set; }
        
        [JsonProperty(PropertyName = "moduleType")]        
        public String ModuleType { get; set; }

        [JsonProperty(PropertyName = "signals")]        
        public IEnumerable<Signal> Signals { get { return _signals.AsEnumerable(); } }

        private readonly object _lockObject = new object();
        private readonly object _lockObjectForSetValue = new object();

        public void RegisterSignal(string type, ushort index, string signalName)
        {
            lock (_lockObject)
            {
                if (_signals.Any(o => o.SignalName.Equals(signalName)))
                    throw new CaviaException(String.Format("A signal '{0}' already exist for module '{1}'.", signalName,
                        ModuleName));

                _signals.Add(new Signal
                {
                    Type = type,
                    Index = index,
                    SignalName = signalName
                });
            }
        }

        public void DoAction(string signalName, string action)
        {
            Signal signal;
            lock (_lockObject)
            {
                // We do not update the value now.
                // It is the SIMPL+ Module the will call SetValue() when it succesfull
                // processes the action.
                signal = _signals.Single(o => o.SignalName == signalName);
            }            
            OnSignalUpdated(this, new SignalUpdatedEventArgs
                {
                    SignalType = signal.Type,
                    SignalName = signal.SignalName,
                    SignalIndex = signal.Index,
                    Action = action
                });
            
        }  


        public void SetValue(string outputName, string value)
        {
            lock (_lockObjectForSetValue)
            {
                SetValueGuard(outputName);
                _signals.Single(o => o.SignalName.Equals(outputName)).Value = value;
            }
        }

        private void SetValueGuard(string outputName)
        {
            if (!_signals.Any(o => o.SignalName.Equals(outputName)))
                throw new CaviaException(
                    String.Format("No signal '{0}' found on module '{1}'.", outputName, ModuleName));
        }
    }
}