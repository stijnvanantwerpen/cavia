﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crestron.EMEA.Cavia.Http;
using Crestron.EMEA.Cavia.IoC;

namespace Crestron.EMEA.Cavia.Cavia.Core.Imp
{
    public class CaviaCoreImp : CaviaCore
    {      

        private readonly List<CaviaModule> _caviaModules = new List<CaviaModule>();
        public CaviaFactory CaviaFactory { get; private set; }
        public CaviaServer CaviaServer { get; private set; }


        public IEnumerable<CaviaArea> Areas {
            get
            {
                var areas = _caviaModules.GroupBy(m => m.AreaName, m => m,
                    (k, g) => new CaviaAreaImp (k,g.ToList()));
                var asInterface = areas.Select(a => a as CaviaArea);
                return asInterface;
            }
        }

        public void RegisterModule(CaviaModule caviaModule)
        {
            _caviaModules.Add(caviaModule);
        }
       
        public void DoAction(string areaName, string moduleName, string action, string value)
        {
            var module = _caviaModules.SingleOrDefault(m => m.AreaName == areaName && m.ModuleName == moduleName);
            if(module == null) throw new CaviaException(
                String.Format("Signal '{0}' not found on module '{1}' in area '{2}'.", action, moduleName, areaName));
            module.DoAction(action, value);
        }

        public void SetFactory(CaviaFactory caviaFactory)
        {
            CaviaFactory = caviaFactory;
        }

        public void SetServer(CaviaServer caviaServer)
        {
            CaviaServer = caviaServer;
        }
    }
}