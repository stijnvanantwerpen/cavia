﻿using Crestron.EMEA.Cavia.DataLayer;

namespace Crestron.EMEA.Cavia.Cavia.SDK
{
    public interface CaviaAuthenticationProvider
    {
        bool CanAuthenticate(HttpRequestArgs httpRequestArgs);
        UserDTO Authenticate(HttpRequestArgs httpRequestArgs);
    }
}