using Crestron.EMEA.Cavia.Cavia.Core;

namespace Crestron.EMEA.Cavia.Cavia.SDK
{
    public interface CaviaAreaExtender
    {
        string GetDescription(CaviaArea caviaArea);        
    }
}