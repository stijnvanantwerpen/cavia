﻿using System;
using Crestron.EMEA.Cavia.Cavia.SDK;
using Crestron.EMEA.Cavia.DataLayer;

namespace Crestron.EMEA.Cavia.Cavia.Authentication
{
    public class DefaultAuthenticationProvider : CaviaAuthenticationProvider
    {
        public bool CanAuthenticate(HttpRequestArgs httpRequestArgs)
        {
            return true;
        }

        public UserDTO Authenticate(HttpRequestArgs httpRequestArgs)
        {
            return new UserDTO
            {                
                FilterByAreaNames = false,
                AreaNames = String.Empty,
                IsAdmin = false,
                IsGuest = true,                
                UserAllowed = true,
                UserId = 0
            };
        }       
    }
}