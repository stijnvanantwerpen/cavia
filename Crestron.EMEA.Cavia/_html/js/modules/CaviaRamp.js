var cavia = cavia || {};
cavia.modules = cavia.modules || {};
cavia.modules.caviaramp = cavia.modules.caviaramp || (function(ng){
	var caviaApp = ng.module('caviaApp');

	caviaApp.controller('CaviaRampController', ['$scope', 'caviaCore', function($scope, caviaCore) {
	    var looping = false;
	    var promis;
	    var startLoopingDelay = 500;
	    var repeatLoopingTime = 100;
	    $scope.getCurrentValue = function() {
	        var rawValue = $scope.module.signals['CurrentValue'].value;
	        return Math.floor((rawValue / 65535) * 100 * 100) / 100;
	    };	
	    $scope.Pulse = function(direction) {
	        $scope.module.signals[direction].Pulse();
	    };
	    $scope.startLooping = function(direction) {
	        looping = true;
	        promis = setTimeout(function() { loop(direction); }, startLoopingDelay);
	    };
	    function loop(direction) {
	        $scope.Pulse(direction);
	        if (looping) {
	            promis = setTimeout(function() { loop(direction); }, repeatLoopingTime);
	        }
	    }
	    $scope.stopLooping = function() {
	        looping = false;
	        clearTimeout(promis);
	    };			    	    
    } ]);		
})(angular);