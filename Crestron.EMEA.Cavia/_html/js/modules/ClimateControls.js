var cavia = cavia || {};
cavia.modules = cavia.modules || {};
cavia.modules.climate = cavia.modules.climate || (function(ng){	
	var caviaApp = ng.module('caviaApp');
	
	caviaApp.controller('ClimateControlsController', ['$scope','caviaCore', function($scope){				
		$scope.modes = ['Fan','Heat','Cool','Auto'];
		$scope.fanSpeeds = ['Low','Medium','High'];						
		$scope.onModeChanged = updateMode;
		$scope.onFanSpeedChanged = updateFanSpeed;
		$scope.getSetPoint = function(){
			var rawValue = $scope.module.signals['SetPoint'].value;
			return Math.floor((rawValue / 65535) * 100 * 100) / 100;
        };		
	    $scope.ChangeSetPoint = function(direction){			
			$scope.module.signals['SetPoint_'+direction].Pulse();
		};
	    var looping = false;
		var promis;
		$scope.startLooping = function(direction){
			looping = true;
			promis = setTimeout(function(){ loop(direction); },1000);			
		};
	    function loop(direction){											
			$scope.ChangeSetPoint(direction);
			if(looping){
				promis = setTimeout(function(){loop(direction); } ,300);
			}
		}		
		$scope.stopLooping = function(){
			looping = false;
			clearTimeout(promis);        };		
	    $scope.changeMode = function (to){			
			$scope.module.mode = to;
			$scope.onModeChanged();
		};
	    $scope.changeFanSpeed = function (to){			
			$scope.module.fanSpeed = to;
			$scope.onFanSpeedChanged();
		};
	    createOnValueChangedEventHandlersForModes();
		createOnValueChangedEventHandlersForFanSpeed();
				
		function updateMode(){
			unsetAllModes();
			//Since post are handled asynchronouse the 'set' sometimes occuers before the 'unset'
			//Therefor we send the set with some delay.
			//TODO: get promis and update when resolved
			setTimeout(function(){ $scope.module.signals['Mode_'+ $scope.module.mode].Set(); },200);
		}
		
		function updateFanSpeed(){			
			unsetAllFanSpeed();
			setTimeout(function(){ $scope.module.signals['Speed_'+ $scope.module.fanSpeed].Set(); },200);;
		}
		
		
		function createOnValueChangedEventHandlersForModes(){
			$scope.modes.forEach(function(mode){
				var signal = $scope.module.signals['Mode_'+ mode];
				signal.onValueChanged = function (s){
					updateModeForSignal(s, mode);				
				};
			});
		}
		
		function createOnValueChangedEventHandlersForFanSpeed(){
			$scope.fanSpeeds.forEach(function(mode){
				var signal = $scope.module.signals['Speed_'+ mode];
				signal.onValueChanged = function (s){
					updateFanSpeedForSignal(s, mode);				
				};
			});
		}
		
		function unsetAllModes(){
			$scope.modes.forEach(function(mode){
				$scope.module.signals['Mode_'+ mode].Unset();
			});
		}
		
		function unsetAllFanSpeed(){
			$scope.fanSpeeds.forEach(function(fanSpeed){
				$scope.module.signals['Speed_'+ fanSpeed].Unset();
			});
		}

		function updateModeForSignal(signal, mode){
			if(signal.value == "1"){
				$scope.module.mode = mode;
			}
		}
		
		function updateFanSpeedForSignal(signal, fanSpeed){
			if(signal.value == "1"){
				$scope.module.fanSpeed = fanSpeed;				
			}
		}			
	}]);	
})(angular);