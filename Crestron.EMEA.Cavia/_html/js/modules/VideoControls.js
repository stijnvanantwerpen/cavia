var cavia = cavia || {};
cavia.modules = cavia.modules || {};
cavia.modules.video = cavia.modules.video || (function(ng) {
    var caviaApp = ng.module('caviaApp');

    caviaApp.controller('VideoControlsController', ['$scope', 'caviaCore', function($scope, caviaCore) {
        $scope.sources = getSources();
        var index = $scope.module.signals['Source Select'].value;
        $scope.module.source = $scope.sources[index];

        $scope.changeSource = function(to) {
            $scope.module.source = to;
            var i = findIndexForSource(to);
            $scope.module.signals['Source Select'].Set(i.toString());
        }

        $scope.powerOn = function() {
            $scope.module.signals['TV Off'].Unset();
            $scope.module.signals['TV On'].Set();
        }

        $scope.powerOff = function() {
            $scope.module.signals['TV On'].Unset();
            $scope.module.signals['TV Off'].Set();
        }

        function getSources() {
            var result = Array();
            for (var i = 1; i <= 16; i++) {
                var sourceName = $scope.module.signals['Source' + i].value;
                if (sourceName != "" && sourceName != "0d") {
                    result.push(sourceName);
                }
            }
            return result;
        }

        function findIndexForSource(source) {
            for (var i = 1; i <= 16; i++) {
                var sourceName = $scope.module.signals['Source' + i].value;
                if (sourceName == source) {
                    return i;
                }
            }
            return 0;
        }
    } ]);
})(angular);