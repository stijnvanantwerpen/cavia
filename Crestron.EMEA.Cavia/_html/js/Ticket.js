var cavia = cavia || {};
cavia.ticket = cavia.ticket || (function(ng){	
	var ticketApp = ng.module('ticketApp', []);	

	ticketApp.controller('TicketController', ['$scope', '$http', function($scope, $http){
		$scope.ticket = getTicket($scope, $http);
	}]);

	function getTicket($scope, $http){
		$http.get('./login')
			.success(function(response){
				var authToken = response.ticket;			
				$http.get('./ticket', { 
					headers: {'caviaAuthenticationToken': authToken } })
					.success(function(ticket){					
						$scope.ticket = ticket;
					});				
			});	
	};

})(angular);