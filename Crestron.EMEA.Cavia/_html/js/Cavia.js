var cavia = cavia || {};
cavia.demo = cavia.demo || (function(ng){	
	var caviaApp = ng.module('caviaApp');	
		
	caviaApp.controller('AreaController', ['$scope','$http','$q','caviaCore', function($scope, $http, $q, caviaCore){
		function load(){
			caviaCore.loadBuilding(function(building){				
				$scope.building = building;	
				$scope.selectedArea = $(building.areas).first().get(0).areaName;	
				addFunctions(building);				
			});
		};

		function addFunctions(building) {
		    var templateMap = {};
			building.areas.forEach(function(area){
				area.modules.forEach(function(module){
				    var moduleTemplateUrl = 'moduleTemplates/' + module.moduleType + '.html';
				    module.getTemplateUrl = function() {
				        return templateMap[moduleTemplateUrl];
				    };
				    if (!(moduleTemplateUrl in templateMap)) {
				        templateMap[moduleTemplateUrl] = 'moduleTemplates/default.html';
				        $http.get(moduleTemplateUrl).then(
				            function(response) {
				                templateMap[moduleTemplateUrl] = moduleTemplateUrl;				                
				            });
				    }
				});
			});			
		};
			
		function updateSignals() {
			caviaCore.updateAllSignals();
		};
					
		(function(){
			caviaCore.login(function(){
				load();
				(function(){											
					updateSignals();
					setTimeout(arguments.callee,1000);				
				})();
			});
		})();	
	}]);

})(angular);		

