var cavia = cavia || {};
cavia.core = cavia.core || (function(ng) {
    var caviaApp = ng.module('caviaApp', []);
    var building;
    var authToken;

    caviaApp.factory('caviaCore', ['$window', '$http', function($window, $http) {
        function login(onSucces) {
            $http.get('./login', { 'headers': { 'caviaPageHref': location.href } })
			.success(function(response) {
			    authToken = response.ticket;
			    onSucces();
			});
        }

        function loadBuilding(withBuildingDo) {
            $http.get('./areas', { 'headers': { 'caviaAuthenticationToken': authToken} })
				.success(function(areas) {
				    building = {};
				    building.areas = areas;
				    areas.forEach(function(area) {
				        areas[area.areaName] = area;
				        area.modules.forEach(function(module) {
				            area.modules[module.moduleName] = module;
				            module.signals.forEach(function(signal) {
				                module.signals[signal.signalName] = (function() {
				                    signal.Do = function(func) {
				                        func(area, module, signal);
				                    };
				                    addFunctions(signal);
				                    return signal;
				                })();
				            });
				        });
				    });
				    withBuildingDo(building);
				});
        };

        function updateAllSignals() {
            $http.get('./areas', { 'headers': { 'caviaAuthenticationToken': authToken} })
				.success(function(areas) {
				    areas.forEach(function(area) {
				        area.modules.forEach(function(module) {
				            module.signals.forEach(function(signal) {
				                var s = building.areas[area.areaName].modules[module.moduleName].signals[signal.signalName];
				                s.value = signal.value;
				                if (typeof s.onValueChanged === "function") {
				                    s.onValueChanged(s);
				                }
				            });
				        });
				    });
				});
        };

        function update(areaName, moduleName, signalName, action) {
            var url = "./areas/" + areaName + "/" + moduleName + "/" + signalName;
            var body = { 'action': action };
            $http.post(url, body).then(updateAllSignals());
        }

        function addFunctions(signal) {
            signal.Send = function(action) {
                signal.Do(function(area, module, s) {
                    update(area.areaName, module.moduleName, s.signalName, action);
                });
            };

            if (signal.type == 'Digital') { addFunctionsOnDigital(signal); }
            if (signal.type == 'Analog') { addFunctionsOnAnalog(signal); }
            if (signal.type == 'Serial') { addFunctionsOnSerial(signal); }
        }

        function addFunctionsOnDigital(signal) {
            signal.Set = function() { signal.Send("set"); };
            signal.Unset = function() { signal.Send("unset"); };
            signal.Pulse = function() { signal.Send("pulse"); };
            signal.Reset = function() { signal.Send("reset"); };
        }
        
        function addFunctionsOnAnalog(signal) {
            signal.Clear = function() { signal.Send("clear"); };
            signal.Full = function() { signal.Send("full"); };
            signal.Increase = function() { signal.Send("increase"); };
            signal.Decrease = function() { signal.Send("decrease"); };
            signal.Reset = function() { signal.Send("reset"); };
            signal.Set = function(value) { signal.Send(value); };
        }
        
        function addFunctionsOnSerial(signal) {
            signal.Set = function(txt) { signal.Send(txt); };
        }
   
        return {
            login: login,
            loadBuilding: loadBuilding,
            updateAllSignals: updateAllSignals
        };
    } ]);
})(angular);