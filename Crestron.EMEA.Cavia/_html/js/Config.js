var cavia = cavia || {};
cavia.config = cavia.config || (function(ng){
var configApp = ng.module('configApp', []);

    configApp.controller('ConfigController', ['$scope', '$http', function($scope, $http) {
		$scope.ticket = getTicket($scope, $http);
	}]);

	function getTicket($scope, $http){
		$http.get('./login')
			.success(function(response){
				var authToken = response.ticket;			
				$http.get('./config', { 
					headers: {'caviaAuthenticationToken': authToken } })
					.success(function(config){					
						$scope.config = config;
					});				
			});	
	};

})(angular);