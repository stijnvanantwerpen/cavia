﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crestron.EMEA.Cavia.Cavia.Core.Imp;
using Crestron.EMEA.SimplUnit;
using Crestron.SimplSharpPro.CrestronThread;

namespace Crestron.EMEA.Cavia.SimplTests
{
    public class CaviaSimplPlusFacade_Threading
    {
        [Fact]
        public void AddServerAndModulesOnDifferentThreads()
        {
            var sutFacade = new CaviaSimplPlusFacade();

            var iEnumerable = RegisterCaviaModules(sutFacade);
            var threads = iEnumerable as Thread[] ?? iEnumerable.ToArray();

            var serverThread = InitServer(sutFacade);           

            foreach (var thread in threads)
            {
                thread.Join();
            }
            serverThread.Join();
           
        }

        private static Thread InitServer(CaviaSimplPlusFacade sutFacade)
        {
            var rnd = new Random();
            return new Thread(specific =>
            {
                Thread.Sleep(rnd.Next(600) + 1200);
                //CrestronConsole.PrintLine("Server.Init()");
                sutFacade.Server.Init(8080, @"\HTML\Cavia\Html", "", "");
                //CrestronConsole.PrintLine("Server.Init() finished");
                return null;
            }, null);

           
        }

        private static IEnumerable<Thread> RegisterCaviaModules(CaviaSimplPlusFacade sutFacade)
        {
            var rnd = new Random();
            return Enumerable.Range(0, 15).Select(x => new Thread(nr =>
            {
                Thread.Sleep(rnd.Next(1000)+1000);
                //CrestronConsole.PrintLine("Registering cavia Module {0}", nr);
                var caviaModule = new CaviaModuleImp
                {
                    AreaName = "Area",
                    ModuleName = "Module " + nr,
                    ModuleType = "Type"                   
                };
                caviaModule.RegisterSignal("sigType",0,"signalName");

                sutFacade.RegisterCaviaModule(caviaModule);
                //CrestronConsole.PrintLine("Registering cavia Module {0} finished", nr);
                return null;
            }, x));
        }
    }
}

